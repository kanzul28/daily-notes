FROM openjdk:17-alpine
WORKDIR /app

COPY target/*.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]

#run docker build -t daily-notes .