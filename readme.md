# daily-notes

Daily Notes is a application to help log activity of the day.

## User Stories

You can see the user story here [readme-userstory.md](readme-userstory.md).

## Entity Relationship Diagram

![erd](readme_asset/erd.png)

## Quick Start

Test hit API login:

```shell
curl --location 'http://localhost:8070/api/v1/user/signup' \
--header 'Content-Type: application/json' \
--data '{
    "username": "user",
    "password": "user123"
}'
```

## API Documentation

For API documentation, you can import the file [postman/daily-notes.postman_collection.json](postman/daily-notes.postman_collection.json) into your Postman.