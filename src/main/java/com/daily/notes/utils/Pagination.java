package com.daily.notes.utils;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Pagination {
    private int offset;
    private int pageSize;
    private int pageNumber;
    private int totalPages;
    private int totalElements;
}
