package com.daily.notes.utils;

import com.daily.notes.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.http.HttpStatus;


@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    private String code;
    private Status status;
    private String message;
    private Object data;
    @JsonIgnore
    private HttpStatus httpStatus;

    public Response success(Object data){
        this.code = "200";
        this.status = Status.success;
        this.data = data;
        this.httpStatus = HttpStatus.OK;
        return this;
    }

    public Response success(String message){
        this.code = "200";
        this.status = Status.success;
        this.message = message;
        this.httpStatus = HttpStatus.OK;
        return this;
    }

    public Response success(Object data, String message){
        this.code = "200";
        this.status = Status.success;
        this.data = data;
        this.message = message;
        this.httpStatus = HttpStatus.OK;
        return this;
    }

    public Response error(String message){
        this.code = "500";
        this.status = Status.error;
        this.message= message;
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        return this;
    }

    public Response error(String message, String code){
        this.code = code;
        this.status = Status.error;
        this.message = message;
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        return this;
    }

    public Response error(String message, String code, HttpStatus httpStatus){
        this.code = code;
        this.status = Status.error;
        this.message = message;
        this.httpStatus = httpStatus;
        return this;
    }





}
