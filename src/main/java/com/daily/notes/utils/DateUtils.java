package com.daily.notes.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
public class DateUtils {

    public static final String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String STANDARD_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final ThreadLocal<SimpleDateFormat> SDF_ISO_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat(ISO_FORMAT));
    public static final ThreadLocal<SimpleDateFormat> SDF_ISO8601_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat(ISO8601_FORMAT));
    public static final ThreadLocal<SimpleDateFormat> SDF_STANDARD_DATE_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat(STANDARD_DATE_FORMAT));

    public static final ZoneOffset ZONE_ID = ZoneOffset.of("+07:00");

    public static String dateToISOISO8601String(Date date) {
        return Optional.ofNullable(date).map(SDF_ISO8601_FORMAT.get()::format).orElse(null);
    }

    public static Date isoString8601ToDate(String strDate) {
        if (StringUtils.isEmpty(strDate)) return null;

        try {
            return SDF_ISO_FORMAT.get().parse(strDate);
        } catch (ParseException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public static LocalDateTime dateToLocalDateTime(Date date) {
        return date.toInstant().atZone(DateUtils.ZONE_ID).toLocalDateTime();
    }

    public static LocalDateTime isoDateToIso8601(Date date){
        return Stream.of(date)
                .map(DateUtils::dateToISOISO8601String)
                .map(DateUtils::isoString8601ToDate)
                .filter(Objects::nonNull)
                .map(DateUtils::dateToLocalDateTime)
                .findFirst()
                .orElse(null);
    }

}
