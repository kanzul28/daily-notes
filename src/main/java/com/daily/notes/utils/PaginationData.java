package com.daily.notes.utils;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Accessors(chain = true)
public class PaginationData<T> {
    private List<T> content;
    private Pagination pagination;
}
