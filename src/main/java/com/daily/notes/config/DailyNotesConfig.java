package com.daily.notes.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DailyNotesConfig {

    @Bean(name = "ds-daily-notes")
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .url("jdbc:mysql://localhost:3306/my_database")
                .username("root")
                .password("password")
                .type(HikariDataSource.class)
                .build();
    }

    @Bean("jdbc-daily-notes")
    public NamedParameterJdbcTemplate jdbcTemplate(@Qualifier("ds-daily-notes") DataSource datasource) {
        return new NamedParameterJdbcTemplate(datasource);
    }

}
