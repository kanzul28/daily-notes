package com.daily.notes.config;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Config {

    private String QUERY_INSERT_USER = "INSERT INTO PUBLIC.\"user\" (id, username, \"password\", " +
            "created_date) " +
            "values (:id, :username, :password, current_timestamp)";

    private String QUERY_FIND_USER_BY_USERNAME = "SELECT id, username, \"password\", created_date, updated_date FROM PUBLIC.\"user\" WHERE username = :username ORDER BY CREATED_DATE DESC";

    private String QUERY_INSERT_NOTES = "INSERT INTO PUBLIC.notes (id, user_id, title, \"content\", " +
            "category_id, created_date) " +
            "values (:id, :user_id, :title, :content, :category_id, current_timestamp)";

    private String QUERY_FIND_NOTES_BY_ID_AND_USERID = "SELECT id, user_id, title, \"content\", category_id, created_date, updated_date FROM PUBLIC.notes where id=:id and user_id=:userId ORDER BY CREATED_DATE DESC";

    private String QUERY_UPDATE_NOTES = "UPDATE notes SET title = :title, \"content\" = :content, category_id = :category_id, updated_date = current_timestamp where id=:id and user_id=:user_id";

    private String QUERY_UPDATE_NOTES_CATEGORY_BY_CATEGORY_ID = "UPDATE notes SET category_id = null, updated_date = current_timestamp where category_id=:categoryId";

    private String QUERY_FIND_NOTES= "SELECT id, user_id, title, \"content\", category_id, created_date, updated_date FROM PUBLIC.notes ORDER BY CREATED_DATE DESC LIMIT :limit OFFSET :offset";

    private String QUERY_FIND_USER_NOTES= "select n.id, n.title, n.\"content\", u.id as user_id, u.username, c.id as category_id, c.\"name\" as category_name, n.created_date, n.updated_date from notes as n " +
            "join public.\"user\" as u on n.user_id = u.id " +
            "left join public.category as c on n.category_id = c.id " +
            "ORDER BY n.CREATED_DATE DESC LIMIT :limit OFFSET :offset";

    private String QUERY_FIND_NOTES_BY_ID= "SELECT id, user_id, title, \"content\", category_id, created_date, updated_date FROM PUBLIC.notes WHERE id = :id  ORDER BY CREATED_DATE DESC";

    private String QUERY_FIND_NOTES_BY_CATEGORY_ID= "SELECT id, user_id, title, \"content\", category_id, created_date, updated_date FROM PUBLIC.notes WHERE category_id = :categoryId  ORDER BY CREATED_DATE DESC";

    private String QUERY_DELETE_NOTES_BY_ID= "DELETE FROM PUBLIC.notes WHERE id = :id ";

    private String QUERY_FIND_USER_NOTES_BY_ID= "select n.id, n.title, n.\"content\", u.id as user_id, u.username, c.id as category_id, c.\"name\" as category_name, n.created_date, n.updated_date from notes as n " +
            "join public.\"user\" as u on n.user_id = u.id " +
            "left join public.category as c on n.category_id = c.id " +
            "where n.id = :id " +
            "order by n.created_date desc";

    private String QUERY_FIND_USER_NOTES_BY_DATE= "select n.id, n.title, n.\"content\", u.id as user_id, u.username, c.id as category_id, c.\"name\" as category_name, n.created_date, n.updated_date from notes as n " +
            "join public.\"user\" as u on n.user_id = u.id " +
            "left join public.category as c on n.category_id = c.id " +
            "where n.CREATED_DATE BETWEEN :startDate AND :endDate " +
            "order by n.created_date desc";

    private String QUERY_INSERT_CATEGORY = "INSERT INTO category (id, name, user_id, created_date) " +
            "values (:id, :name, :userId, current_timestamp)";

    private String QUERY_FIND_CATEGORY_BY_ID = "select id, \"name\" from category where id = :id";

    private String QUERY_DELETE_CATEGORY_BY_ID = "delete from category where id = :id";

    private String QUERY_UPDATE_CATEGORY = "UPDATE category SET \"name\" = :name, updated_date = current_timestamp where id=:id";

    private String QUERY_FIND_CATEGORY_BY_USER_ID = "select id, \"name\" from category where id = :userId";

    private String QUERY_FIND_USER_CATEGORY_BY_ID = "select c.id, c.\"name\", u.id as user_id, u.username from category as c " +
            "join \"user\" as u on  c.user_id = u.id " +
            "where c.id = :id " +
            "order by c.created_date desc";

    private String QUERY_FIND_USER_CATEGORY_BY_USER_ID = "select c.id, c.\"name\", u.id as user_id, u.username from category as c " +
            "join \"user\" as u on  c.user_id = u.id " +
            "where c.user_id = :userId " +
            "order by c.created_date desc";

    private String QUERY_FIND_USER_HISTORY_BY_NOTES_ID = "select h.id, h.title, h.\"content\", u.id as user_id, u.username, h.created_date from history as h " +
            "join \"user\" as u on  h.user_id = u.id " +
            "where h.notes_id = :notesId and h.user_id = :userId " +
            "order by h.created_date desc LIMIT :limit OFFSET :offset";

    private String QUERY_FIND_USER_HISTORY_BY_ID = "select h.id, h.title, h.\"content\", u.id as user_id, u.username, h.created_date from history as h " +
            "join \"user\" as u on  h.user_id = u.id " +
            "where h.id = :id " +
            "order by h.created_date desc";

    private String QUERY_INSERT_HISTORY = "INSERT INTO history (id, title, content, notes_id, user_id, created_date) " +
            "values (:id, :title, :content, :notesId, :userId, current_timestamp)";

    private String QUERY_DELETE_HISTORY_BY_NOTES_ID = "DELETE FROM history where history.notes_id = :notesId";

}
