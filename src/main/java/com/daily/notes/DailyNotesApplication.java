package com.daily.notes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyNotesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyNotesApplication.class, args);
	}

}
