package com.daily.notes.event;

import com.daily.notes.enums.EventEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.context.ApplicationEvent;

import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class CustomEvent extends ApplicationEvent {

    private Map<String, Object> event;

    private EventEnum eventEnum;

    public CustomEvent(Object source) {
        super(source);
    }
}
