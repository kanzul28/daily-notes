package com.daily.notes.event;

import com.daily.notes.internal.history.service.HistoryService;
import com.daily.notes.internal.notes.model.entity.Notes;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class HistoryEventListener {

    private final HistoryService historyService;

    private final String REQUEST_RECEIVED = "[{}][EVENT RECEIVED]";

    @EventListener(condition = "(#event.eventEnum.name() == 'INSERT_NOTES')")
    public void handleEventInsertNotes(CustomEvent event){
        try{
            log.info(REQUEST_RECEIVED, "handleEventInsertNotes");
            historyService.insertHistory((Notes) event.getEvent().get("history"), (String) event.getEvent().get("username"));
        }catch(Exception e){
            log.error("error insert history: {}", e);
        }
    }

    @EventListener(condition = "(#event.eventEnum.name() == 'UPDATE_NOTES')")
    public void handleEventUpdateNotes(CustomEvent event){
        try{
            log.info(REQUEST_RECEIVED, "handleEventUpdateNotes");
            historyService.insertHistory((Notes) event.getEvent().get("history"), (String) event.getEvent().get("username"));
        }catch(Exception e){
            log.error("error insert history: {}", e);
        }
    }
}
