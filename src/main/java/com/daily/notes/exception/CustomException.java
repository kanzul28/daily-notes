package com.daily.notes.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class CustomException extends Exception{

    private String message;
    private String code;
    private HttpStatus httpStatus;

    public CustomException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public CustomException(String message, String code, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
        this.code = code;
    }


}
