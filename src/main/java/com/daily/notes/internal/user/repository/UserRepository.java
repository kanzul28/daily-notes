package com.daily.notes.internal.user.repository;

import com.daily.notes.config.Config;
import com.daily.notes.internal.user.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public class UserRepository {

    @Autowired
    private Config config;
    @Autowired
    @Qualifier("jdbc-daily-notes")
    protected NamedParameterJdbcTemplate jdbcTemplate;

    public int insertUserCredential(User data) {
        String sql = config.getQUERY_INSERT_USER();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", UUID.randomUUID().toString());
        parameterSource.addValue("username", data.getUsername());
        parameterSource.addValue("password", data.getPassword());

        return jdbcTemplate.update(sql, parameterSource);
    }

    public List<User> getByUsername(String username) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("username", username);

        return jdbcTemplate.query(config.getQUERY_FIND_USER_BY_USERNAME(), parameterSource, new BeanPropertyRowMapper<>(User.class));
    }
}
