package com.daily.notes.internal.user.service;

import com.daily.notes.exception.CustomException;
import com.daily.notes.internal.user.model.entity.User;
import com.daily.notes.internal.user.model.request.UserRequest;
import com.daily.notes.internal.user.model.response.AuthResponse;
import com.daily.notes.internal.user.repository.UserRepository;
import com.daily.notes.security.jwt.JwtTokenUtils;
import com.daily.notes.utils.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenUtils jwtTokenUtils;
    private final AuthenticationManager authenticationManager;


    public Response signin(UserRequest request) {
        Response response = new Response();
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            String token =  jwtTokenUtils.generateToken(request.getUsername());
            AuthResponse authResponse = new AuthResponse().setAccessToken(token);
            return response.success(authResponse);
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response signup(UserRequest request) {
        Response response = new Response();
        try{
            if (userRepository.getByUsername(request.getUsername()).isEmpty()) {
                request.setPassword(passwordEncoder.encode(request.getPassword()));
                User user = new User()
                        .setUsername(request.getUsername())
                        .setPassword(request.getPassword());
                userRepository.insertUserCredential(user);
                String token = jwtTokenUtils.generateToken(request.getUsername());
                AuthResponse authResponse = new AuthResponse().setAccessToken(token);
                return response.success(authResponse);
            }else{
                throw new CustomException("Username is already in use",HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("Error Username is already in use {}", e);
            return response.error(e.getMessage());
        }
    }

    public String refresh(String username) {
        return jwtTokenUtils.generateToken(username);
    }

}
