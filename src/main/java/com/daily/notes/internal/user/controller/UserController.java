package com.daily.notes.internal.user.controller;

import com.daily.notes.internal.user.model.request.UserRequest;
import com.daily.notes.internal.user.service.UserService;
import com.daily.notes.utils.Response;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RestController
@Validated
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private final String REQUEST_RECEIVED = "[{}][REQUEST RECEIVED][{}]";
    private final String REQUEST_END = "[{}][REQUEST END][{}]";


    @Operation(summary = "Login User", operationId = "loginUser", description = "")
    @RequestMapping(method = RequestMethod.POST, value = "/user/login")
    public ResponseEntity<Response> login(@RequestBody @Valid UserRequest request,
                                     HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "loginUser", servletRequest.getQueryString());
        Response body =  userService.signin(request);
        log.info(REQUEST_END, "loginUser",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Register User", operationId = "registerUser", description = "")
    @RequestMapping(method = RequestMethod.POST, value = "/user/signup")
    public ResponseEntity<Response> signup(@RequestBody @Valid UserRequest request,
                                     HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "registerUser", servletRequest.getQueryString());
        Response body =  userService.signup(request);
        log.info(REQUEST_END, "registerUser", body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

}
