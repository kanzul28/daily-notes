package com.daily.notes.internal.user.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserRequest {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
