package com.daily.notes.internal.history.model.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ContentChange {
    private String title;
    private String content;
}
