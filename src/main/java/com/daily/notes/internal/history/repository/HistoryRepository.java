package com.daily.notes.internal.history.repository;

import com.daily.notes.config.Config;
import com.daily.notes.internal.history.model.entity.History;
import com.daily.notes.internal.history.model.entity.NotesHistory;
import com.daily.notes.internal.notes.model.entity.Notes;
import com.daily.notes.internal.notes.model.entity.UserNotes;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class HistoryRepository {
    private final Config config;

    @Autowired
    @Qualifier("jdbc-daily-notes")
    protected NamedParameterJdbcTemplate jdbcTemplate;

    public List<NotesHistory> getHistoryByNotesId(int offset, int limit, String notesId, String userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("offset", offset);
        parameterSource.addValue("limit", limit);
        parameterSource.addValue("notesId", notesId);
        parameterSource.addValue("userId", userId);


        return jdbcTemplate.query(config.getQUERY_FIND_USER_HISTORY_BY_NOTES_ID(), parameterSource, new BeanPropertyRowMapper<>(NotesHistory.class));
    }

    public List<NotesHistory> getHistoryById(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.query(config.getQUERY_FIND_USER_HISTORY_BY_ID(), parameterSource, new BeanPropertyRowMapper<>(NotesHistory.class));
    }

    public int insertHistory(History data) {
        String sql = config.getQUERY_INSERT_HISTORY();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", UUID.randomUUID().toString());
        parameterSource.addValue("userId", data.getUserId());
        parameterSource.addValue("title", data.getTitle());
        parameterSource.addValue("content", data.getContent());
        parameterSource.addValue("notesId", data.getNotesId());

        return jdbcTemplate.update(sql, parameterSource);
    }

    public int deleteHistoryByNotesId(String notesId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("notesId", notesId);

        return jdbcTemplate.update(config.getQUERY_DELETE_HISTORY_BY_NOTES_ID(), parameterSource);
    }


}
