package com.daily.notes.internal.history.model.response;

import com.daily.notes.internal.user.model.response.UserResponse;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class HistoryResponse {
    private String id;
    private LocalDateTime createdDate;
    private UserResponse user;
    private ContentChange changes;
}
