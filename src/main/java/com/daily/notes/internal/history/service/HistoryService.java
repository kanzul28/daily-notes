package com.daily.notes.internal.history.service;

import com.daily.notes.config.Constant;
import com.daily.notes.exception.CustomException;
import com.daily.notes.internal.category.model.response.CategoryResponse;
import com.daily.notes.internal.history.model.entity.History;
import com.daily.notes.internal.history.model.entity.NotesHistory;
import com.daily.notes.internal.history.model.request.HistoryRequest;
import com.daily.notes.internal.history.model.response.ContentChange;
import com.daily.notes.internal.history.model.response.HistoryResponse;
import com.daily.notes.internal.history.repository.HistoryRepository;
import com.daily.notes.internal.notes.model.entity.Notes;
import com.daily.notes.internal.user.model.entity.User;
import com.daily.notes.internal.user.model.response.UserResponse;
import com.daily.notes.internal.user.repository.UserRepository;
import com.daily.notes.utils.DateUtils;
import com.daily.notes.utils.Pagination;
import com.daily.notes.utils.PaginationData;
import com.daily.notes.utils.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class HistoryService {

    private final HistoryRepository historyRepository;
    private final UserRepository userRepository;

    public Response insertHistory(Notes request, String username) {
        Response response = new Response();
        try {
            User user = userRepository.getByUsername(username).get(0);
            History history = new History()
                    .setUserId(user.getId())
                    .setTitle(request.getTitle())
                    .setContent(request.getContent())
                    .setNotesId(request.getId());
            historyRepository.insertHistory(history);
            return response.success("Success insert history data");
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response getHistoryByNotesId(int pageNo, int pageSize, String notesId, String username){
        Response response = new Response();
        try {
            pageNo = pageNo <= 0 ? 1 : pageNo;
            int offset = (pageNo - 1) * pageSize;
            String userId = userRepository.getByUsername(username).get(0).getId();
            List<NotesHistory> notesHistoryList = historyRepository.getHistoryByNotesId(offset, pageSize, notesId, userId);

            if(notesHistoryList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);

            int totalElement = notesHistoryList.size();
            int totalPages = (int) Math.ceil((double) totalElement / pageSize);

            List<HistoryResponse> historyResponseList = notesHistoryList.stream()
                    .map(notesHistory -> {
                        UserResponse user = new UserResponse()
                                .setId(notesHistory.getUserId())
                                .setUsername(notesHistory.getUsername());
                        ContentChange changes = new ContentChange()
                                .setTitle(notesHistory.getTitle())
                                .setContent(notesHistory.getContent());

                        return new HistoryResponse()
                                .setId(notesHistory.getId())
                                .setUser(user)
                                .setChanges(changes)
                                .setCreatedDate(DateUtils.isoDateToIso8601(notesHistory.getCreatedDate()));
                    }).toList();

            Pagination pagination = new Pagination()
                    .setOffset(offset)
                    .setPageSize(pageSize)
                    .setPageNumber(pageNo)
                    .setTotalElements(totalElement)
                    .setTotalPages(totalPages);
            PaginationData<HistoryResponse> paginationData = new PaginationData<HistoryResponse>()
                    .setContent(historyResponseList)
                    .setPagination(pagination);
            return response.success(paginationData);
        } catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response getHistoryById(String id) {
        Response response = new Response();
        try {
            List<NotesHistory> notesHistoryList = historyRepository.getHistoryById(id);
            if(notesHistoryList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);
            NotesHistory notesHistory = notesHistoryList.get(0);
            UserResponse user = new UserResponse()
                    .setId(notesHistory.getUserId())
                    .setUsername(notesHistory.getUsername());
            ContentChange changes = new ContentChange()
                    .setTitle(notesHistory.getTitle())
                    .setContent(notesHistory.getContent());

            HistoryResponse historyResponse = new HistoryResponse()
                    .setId(notesHistory.getId())
                    .setUser(user)
                    .setChanges(changes)
                    .setCreatedDate(DateUtils.isoDateToIso8601(notesHistory.getCreatedDate()));
            return response.success(historyResponse);
        }catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        }catch (Exception e){
            return response.error(e.getMessage());
        }
    }
}
