package com.daily.notes.internal.history.model.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class NotesHistory {
    private String id;
    private String notesId;
    private String title;
    private String content;
    private String userId;
    private String username;
    private Date createdDate;
}
