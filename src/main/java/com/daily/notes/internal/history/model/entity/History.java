package com.daily.notes.internal.history.model.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class History {
    private String id;
    private String notesId;
    private String title;
    private String content;
    private String userId;
}
