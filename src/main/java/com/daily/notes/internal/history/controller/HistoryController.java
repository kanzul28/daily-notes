package com.daily.notes.internal.history.controller;

import com.daily.notes.internal.history.service.HistoryService;
import com.daily.notes.internal.notes.service.NotesService;
import com.daily.notes.utils.Response;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class HistoryController {

    private final HistoryService historyService;

    private final String REQUEST_RECEIVED = "[{}][REQUEST RECEIVED][{}]";
    private final String REQUEST_END = "[{}][REQUEST END][{}]";

    @Operation(summary = "Get History By Notes Id", operationId = "getHistoryByNotesId", description = "")
    @RequestMapping(method = RequestMethod.GET, value = "/history/notes/{notesId}")
    public ResponseEntity<Response> getHistoryByNotesId(
            @RequestParam(defaultValue = "1") int pageNo
            , @RequestParam(defaultValue = "10") int pageSize
            , @PathVariable String notesId
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "getHistoryByNotesId", servletRequest.getQueryString());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Response body =  historyService.getHistoryByNotesId(pageNo, pageSize, notesId, username);
        log.info(REQUEST_END, "getHistoryByNotesId",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Get History By Id", operationId = "getHistoryById", description = "")
    @RequestMapping(method = RequestMethod.GET, value = "/history/{id}")
    public ResponseEntity<Response> getHistoryById(
            @PathVariable String id
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "getHistoryById", servletRequest.getQueryString());
        Response body =  historyService.getHistoryById(id);
        log.info(REQUEST_END, "getHistoryById",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }
}
