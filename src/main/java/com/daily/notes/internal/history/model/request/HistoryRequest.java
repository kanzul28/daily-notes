package com.daily.notes.internal.history.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class HistoryRequest {
    private String notesId;
    private String title;
    private String content;
    private String userId;
}
