package com.daily.notes.internal.notes.controller;

import com.daily.notes.internal.notes.model.request.NotesRequest;
import com.daily.notes.internal.notes.model.request.UpdateNotes;
import com.daily.notes.internal.notes.service.NotesService;
import com.daily.notes.internal.user.model.request.UserRequest;
import com.daily.notes.internal.user.service.UserService;
import com.daily.notes.utils.Response;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Validated
public class NotesController {

    private final NotesService notesService;

    private final String REQUEST_RECEIVED = "[{}][REQUEST RECEIVED][{}]";
    private final String REQUEST_END = "[{}][REQUEST END][{}]";

    @Operation(summary = "Insert Notes", operationId = "insertNotes", description = "")
    @RequestMapping(method = RequestMethod.POST, value = "/notes")
    public ResponseEntity<Response> insertNotes(
            @RequestBody NotesRequest request
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "insertNotes", servletRequest.getQueryString());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Response body =  notesService.insertNotes(request, username);
        log.info(REQUEST_END, "insertNotes",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Update Notes", operationId = "updateNotes", description = "")
    @RequestMapping(method = RequestMethod.PUT, value = "/notes")
    public ResponseEntity<Response> updateNotes(
            @RequestBody UpdateNotes request
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "updateNotes", servletRequest.getQueryString());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Response body =  notesService.updateNotes(request, username);
        log.info(REQUEST_END, "updateNotes",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Delete Notes", operationId = "deleteNotes", description = "")
    @RequestMapping(method = RequestMethod.DELETE, value = "/notes/{id}")
    public ResponseEntity<Response> deleteNotes(HttpServletRequest servletRequest
                                , @PathVariable String id) {
        log.info(REQUEST_RECEIVED, "deleteNotes", servletRequest.getQueryString());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Response body =  notesService.deleteNotesById(id, username);
        log.info(REQUEST_END, "deleteNotes",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Get Notes", operationId = "getNotes", description = "")
    @RequestMapping(method = RequestMethod.GET, value = "/notes")
    public ResponseEntity<Response> getNotes(
            @RequestParam(defaultValue = "1") int pageNo
            , @RequestParam(defaultValue = "10") int pageSize
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "getNotes", servletRequest.getQueryString());
        Response body =  notesService.getAllNotes(pageNo, pageSize);
        log.info(REQUEST_END, "getNotes",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Get Notes By Id", operationId = "getNotesById", description = "")
    @RequestMapping(method = RequestMethod.GET, value = "/notes/{id}")
    public ResponseEntity<Response> getNotesById(
            @PathVariable String id
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "getNotesById", servletRequest.getQueryString());
        Response body =  notesService.getNotesById(id);
        log.info(REQUEST_END, "getNotesById",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Get Notes By Date", operationId = "getNotesByDate", description = "")
    @RequestMapping(method = RequestMethod.GET, value = "/notes/byDate")
    public ResponseEntity<Response> getNotesByDate( @RequestParam(defaultValue = "1") int pageNo
            , @RequestParam(defaultValue = "10") int pageSize
            , @Parameter(description = "Start Date of the transaction with format : `yyyy-MM-dd HH:mm:ss`") @Valid @RequestHeader(value = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate
            , @Parameter(description = "End Date of the transaction with format: `yyyy-MM-dd HH:mm:ss`") @Valid @RequestHeader(value = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "getNotesByDate", servletRequest.getQueryString());
        Response body =  notesService.getNotesByDate(pageNo, pageSize, startDate, endDate);
        log.info(REQUEST_END, "getNotesByDate",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }
}
