package com.daily.notes.internal.notes.service;

import com.daily.notes.config.Constant;
import com.daily.notes.enums.EventEnum;
import com.daily.notes.exception.CustomException;
import com.daily.notes.internal.category.model.response.CategoryResponse;
import com.daily.notes.event.CustomEvent;
import com.daily.notes.internal.history.model.entity.History;
import com.daily.notes.internal.history.model.entity.NotesHistory;
import com.daily.notes.internal.history.repository.HistoryRepository;
import com.daily.notes.internal.history.service.HistoryService;
import com.daily.notes.internal.notes.model.entity.Notes;
import com.daily.notes.internal.notes.model.entity.UserNotes;
import com.daily.notes.internal.notes.model.request.NotesRequest;
import com.daily.notes.internal.notes.model.request.UpdateNotes;
import com.daily.notes.internal.notes.model.response.NotesResponse;
import com.daily.notes.internal.notes.repository.NotesRepository;
import com.daily.notes.internal.user.model.entity.User;
import com.daily.notes.internal.user.model.response.UserResponse;
import com.daily.notes.internal.user.repository.UserRepository;
import com.daily.notes.utils.DateUtils;
import com.daily.notes.utils.Pagination;
import com.daily.notes.utils.PaginationData;
import com.daily.notes.utils.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class NotesService {

    private final NotesRepository notesRepository;
    private final UserRepository userRepository;
    private final HistoryRepository historyRepository;
    private final ApplicationEventPublisher eventPublisher;

    public Response insertNotes(NotesRequest request, String username) {
        Response response = new Response();
        Map<String, Object> event = new HashMap<>();
        CustomEvent customEvent = new CustomEvent(this).setEventEnum(EventEnum.INSERT_NOTES);
        try {
            User user = userRepository.getByUsername(username).get(0);
            Notes notes = new Notes()
                    .setId(UUID.randomUUID().toString())
                    .setUserId(user.getId())
                    .setCategoryId(request.getCategoryId())
                    .setContent(request.getContent())
                    .setTitle(request.getTitle());
            notesRepository.insertNotes(notes);
            event.put("username", username);
            event.put("history", notes);
            customEvent.setEvent(event);
            eventPublisher.publishEvent(customEvent);
            return response.success("Success insert notes data");
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response updateNotes(UpdateNotes request, String username) {
        Response response = new Response();
        Map<String, Object> event = new HashMap<>();
        CustomEvent customEvent = new CustomEvent(this).setEventEnum(EventEnum.UPDATE_NOTES);
        try {
            User user = userRepository.getByUsername(username).get(0);
            List<Notes> notesList = notesRepository.getNotesByIdAndUserId(request.getId(), user.getId());
            if(notesList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);
            Notes notes = new Notes()
                    .setId(request.getId())
                    .setUserId(user.getId())
                    .setCategoryId(request.getCategoryId())
                    .setContent(request.getContent())
                    .setTitle(request.getTitle());
            notesRepository.updateNotes(notes);
            event.put("username", username);
            event.put("history", notes);
            customEvent.setEvent(event);
            eventPublisher.publishEvent(customEvent);
            return response.success("Success update notes data");
        } catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response getAllNotes(int pageNo, int pageSize){
        Response response = new Response();
        try {
            pageNo = pageNo <= 0 ? 1 : pageNo;
            int offset = (pageNo - 1) * pageSize;
            List<UserNotes> notesList = notesRepository.getAllUserNotes(offset, pageSize);

            if(notesList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);

            int totalElement = notesList.size();
            int totalPages = (int) Math.ceil((double) totalElement / pageSize);

            List<NotesResponse> notesResponseList = notesList.stream()
                    .map(userNotes -> {
                        UserResponse user = new UserResponse()
                                .setId(userNotes.getUserId())
                                .setUsername(userNotes.getUsername());
                        CategoryResponse category = new CategoryResponse()
                                .setId(userNotes.getCategoryId())
                                .setName(userNotes.getCategoryName());

                        return new NotesResponse()
                                .setId(userNotes.getId())
                                .setTitle(userNotes.getTitle())
                                .setContent(userNotes.getContent())
                                .setCreatedDate(DateUtils.isoDateToIso8601(userNotes.getCreatedDate()))
                                .setUpdatedDate(DateUtils.isoDateToIso8601(userNotes.getUpdatedDate()))
                                .setUser(user)
                                .setCategory(category.getId() == null ? null : category);
                    }).toList();

            Pagination pagination = new Pagination()
                    .setOffset(offset)
                    .setPageSize(pageSize)
                    .setPageNumber(pageNo)
                    .setTotalElements(totalElement)
                    .setTotalPages(totalPages);
            PaginationData<NotesResponse> paginationData = new PaginationData<NotesResponse>()
                    .setContent(notesResponseList)
                    .setPagination(pagination);
            return response.success(paginationData);
        } catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response getNotesById(String id) {
        Response response = new Response();
        try {
            List<UserNotes> notesList = notesRepository.getUserNotesById(id);
            if(notesList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);
            UserNotes notes = notesList.get(0);
            UserResponse user = new UserResponse().setId(notes.getUserId()).setUsername(notes.getUsername());
            CategoryResponse category = new CategoryResponse().setId(notes.getCategoryId()).setName(notes.getCategoryName());
            NotesResponse notesResponse = new NotesResponse()
                    .setId(notes.getId())
                    .setTitle(notes.getTitle())
                    .setContent(notes.getContent())
                    .setCreatedDate(DateUtils.isoDateToIso8601(notes.getCreatedDate()))
                    .setUpdatedDate(DateUtils.isoDateToIso8601(notes.getUpdatedDate()))
                    .setUser(user)
                    .setCategory(category.getId()==null?null:category);
            return response.success(notesResponse);
        }catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        }catch (Exception e){
            return response.error(e.getMessage());
        }
    }

    public Response getNotesByDate(int pageNo, int pageSize, Date startDate, Date endDate){
        Response response = new Response();
        try {
            pageNo = pageNo <= 0 ? 1 : pageNo;
            int offset = (pageNo - 1) * pageSize;
            List<UserNotes> notesList = notesRepository.getNotesByDate(offset, pageSize, startDate, endDate);

            if(notesList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);

            int totalElement = notesList.size();
            int totalPages = (int) Math.ceil((double) totalElement / pageSize);

            List<NotesResponse> notesResponseList = notesList.stream()
                    .map(userNotes -> {
                        UserResponse user = new UserResponse()
                                .setId(userNotes.getUserId())
                                .setUsername(userNotes.getUsername());
                        CategoryResponse category = new CategoryResponse()
                                .setId(userNotes.getCategoryId())
                                .setName(userNotes.getCategoryName());

                        return new NotesResponse()
                                .setId(userNotes.getId())
                                .setTitle(userNotes.getTitle())
                                .setContent(userNotes.getContent())
                                .setCreatedDate(DateUtils.isoDateToIso8601(userNotes.getCreatedDate()))
                                .setUpdatedDate(DateUtils.isoDateToIso8601(userNotes.getUpdatedDate()))
                                .setUser(user)
                                .setCategory(category.getId() == null ? null : category);
                    }).toList();

            Pagination pagination = new Pagination()
                    .setOffset(offset)
                    .setPageSize(pageSize)
                    .setPageNumber(pageNo)
                    .setTotalElements(totalElement)
                    .setTotalPages(totalPages);
            PaginationData<NotesResponse> paginationData = new PaginationData<NotesResponse>()
                    .setContent(notesResponseList)
                    .setPagination(pagination);
            return response.success(paginationData);
        } catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response deleteNotesById(String id, String username){
        Response response = new Response();
        try {
            User user = userRepository.getByUsername(username).get(0);
            List<Notes> notesList = notesRepository.getNotesById(id);
            if (notesList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);

            List<NotesHistory> historyList = historyRepository.getHistoryByNotesId(0, 10, id, user.getId());
            if(!historyList.isEmpty()){
                historyRepository.deleteHistoryByNotesId(id);
            }
            notesRepository.deleteNotesById(id);
            return response.success("Success delete notes data");
        } catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }
}
