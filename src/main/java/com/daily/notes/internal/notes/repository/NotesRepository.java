package com.daily.notes.internal.notes.repository;

import com.daily.notes.config.Config;
import com.daily.notes.internal.notes.model.entity.Notes;
import com.daily.notes.internal.notes.model.entity.UserNotes;
import com.daily.notes.internal.notes.model.request.UpdateNotes;
import com.daily.notes.internal.user.model.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class NotesRepository {

    private final Config config;

    @Autowired
    @Qualifier("jdbc-daily-notes")
    protected NamedParameterJdbcTemplate jdbcTemplate;

    public int insertNotes(Notes data) {
        String sql = config.getQUERY_INSERT_NOTES();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", data.getId());
        parameterSource.addValue("user_id", data.getUserId());
        parameterSource.addValue("title", data.getTitle());
        parameterSource.addValue("content", data.getContent());
        parameterSource.addValue("category_id", data.getCategoryId());

        return jdbcTemplate.update(sql, parameterSource);
    }

    public int updateNotes(Notes data) {
        String sql = config.getQUERY_UPDATE_NOTES();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", data.getId());
        parameterSource.addValue("title", data.getTitle());
        parameterSource.addValue("content", data.getContent());
        parameterSource.addValue("category_id", data.getCategoryId());
        parameterSource.addValue("user_id", data.getUserId());

        return jdbcTemplate.update(sql, parameterSource);
    }

    public int updateNotesCategoryByCategoryId(String categoryId) {
        String sql = config.getQUERY_UPDATE_NOTES_CATEGORY_BY_CATEGORY_ID();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("categoryId", categoryId);

        return jdbcTemplate.update(sql, parameterSource);
    }

    public List<Notes> getNotesByIdAndUserId(String id, String userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);
        parameterSource.addValue("userId", userId);

        return jdbcTemplate.query(config.getQUERY_FIND_NOTES_BY_ID_AND_USERID(), parameterSource, new BeanPropertyRowMapper<>(Notes.class));
    }

    public List<Notes> getNotesById(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.query(config.getQUERY_FIND_NOTES_BY_ID(), parameterSource, new BeanPropertyRowMapper<>(Notes.class));
    }

    public List<Notes> getNotesByCategoryId(String categoryId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("categoryId", categoryId);

        return jdbcTemplate.query(config.getQUERY_FIND_NOTES_BY_CATEGORY_ID(), parameterSource, new BeanPropertyRowMapper<>(Notes.class));
    }

    public List<UserNotes> getUserNotesById(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.query(config.getQUERY_FIND_USER_NOTES_BY_ID(), parameterSource, new BeanPropertyRowMapper<>(UserNotes.class));
    }

    public List<UserNotes> getAllUserNotes(int offset, int limit) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("offset", offset);
        parameterSource.addValue("limit", limit);

        return jdbcTemplate.query(config.getQUERY_FIND_USER_NOTES(), parameterSource, new BeanPropertyRowMapper<>(UserNotes.class));
    }

    public List<Notes> getAllNotes(int offset, int limit) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("offset", offset);
        parameterSource.addValue("limit", limit);

        return jdbcTemplate.query(config.getQUERY_FIND_NOTES(), parameterSource, new BeanPropertyRowMapper<>(Notes.class));
    }

    public List<UserNotes> getNotesByDate(int offset, int limit, Date startDate, Date endDate) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("offset", offset);
        parameterSource.addValue("limit", limit);
        parameterSource.addValue("startDate", startDate);
        parameterSource.addValue("endDate", endDate);

        return jdbcTemplate.query(config.getQUERY_FIND_USER_NOTES_BY_DATE(), parameterSource, new BeanPropertyRowMapper<>(UserNotes.class));
    }

    public int deleteNotesById(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.update(config.getQUERY_DELETE_NOTES_BY_ID(), parameterSource);
    }
}
