package com.daily.notes.internal.notes.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserNotes {
    private String id;
    private String title;
    private String content;
    private String userId;
    private String username;
    private String categoryId;
    private String categoryName;
    private Date createdDate;
    private Date updatedDate;
}
