package com.daily.notes.internal.notes.model.request;

import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
public class NotesRequest {
    private String title;
    private String content;
    private String categoryId;
}
