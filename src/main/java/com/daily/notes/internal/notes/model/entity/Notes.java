package com.daily.notes.internal.notes.model.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class Notes {
    private String id;
    private String userId;
    private String title;
    private String content;
    private String categoryId;
    private Date createdDate;
    private Date updatedDate;
}
