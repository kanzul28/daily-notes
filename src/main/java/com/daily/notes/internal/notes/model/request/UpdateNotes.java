package com.daily.notes.internal.notes.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UpdateNotes {
    @NotBlank
    private String id;
    private String title;
    private String Content;
    private String categoryId;
}
