package com.daily.notes.internal.notes.model.response;

import com.daily.notes.internal.category.model.entity.Category;
import com.daily.notes.internal.category.model.response.CategoryResponse;
import com.daily.notes.internal.user.model.response.UserResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotesResponse {
    private String id;
    private String title;
    private String content;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;
    private UserResponse user;
    private CategoryResponse category;


}
