package com.daily.notes.internal.category.repository;

import com.daily.notes.config.Config;
import com.daily.notes.internal.category.model.entity.Category;
import com.daily.notes.internal.category.model.entity.UserCategory;
import com.daily.notes.internal.notes.model.entity.Notes;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class CategoryRepository {

    private final Config config;

    @Autowired
    @Qualifier("jdbc-daily-notes")
    protected NamedParameterJdbcTemplate jdbcTemplate;


    public int insertCategory(Category data){
        String sql = config.getQUERY_INSERT_CATEGORY();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", UUID.randomUUID().toString());
        parameterSource.addValue("name", data.getName());
        parameterSource.addValue("userId", data.getUserId());

        return jdbcTemplate.update(sql, parameterSource);
    }

    public List<Category> getCategoryById(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.query(config.getQUERY_FIND_CATEGORY_BY_ID(), parameterSource, new BeanPropertyRowMapper<>(Category.class));
    }

    public List<UserCategory> getUserCategoryById(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.query(config.getQUERY_FIND_USER_CATEGORY_BY_ID(), parameterSource, new BeanPropertyRowMapper<>(UserCategory.class));
    }

    public List<UserCategory> getUserCategoryByUserId(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("userId", id);

        return jdbcTemplate.query(config.getQUERY_FIND_USER_CATEGORY_BY_USER_ID(), parameterSource, new BeanPropertyRowMapper<>(UserCategory.class));
    }

    public int deleteCategoryById(String id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.update(config.getQUERY_DELETE_CATEGORY_BY_ID(), parameterSource);
    }

    public int updateCategory(Category data) {
        String sql = config.getQUERY_UPDATE_CATEGORY();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", data.getId());
        parameterSource.addValue("name", data.getName());

        return jdbcTemplate.update(sql, parameterSource);
    }
}
