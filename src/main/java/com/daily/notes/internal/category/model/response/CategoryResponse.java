package com.daily.notes.internal.category.model.response;

import com.daily.notes.internal.user.model.entity.User;
import com.daily.notes.internal.user.model.response.UserResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryResponse {
    private String id;
    private String name;
    private UserResponse user;
}
