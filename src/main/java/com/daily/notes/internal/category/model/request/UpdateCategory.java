package com.daily.notes.internal.category.model.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UpdateCategory {
    private String id;
    private String name;
}
