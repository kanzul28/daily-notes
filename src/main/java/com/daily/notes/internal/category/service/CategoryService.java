package com.daily.notes.internal.category.service;

import com.daily.notes.config.Constant;
import com.daily.notes.enums.EventEnum;
import com.daily.notes.event.CustomEvent;
import com.daily.notes.exception.CustomException;
import com.daily.notes.internal.category.model.entity.Category;
import com.daily.notes.internal.category.model.entity.UserCategory;
import com.daily.notes.internal.category.model.request.CategoryRequest;
import com.daily.notes.internal.category.model.request.UpdateCategory;
import com.daily.notes.internal.category.model.response.CategoryResponse;
import com.daily.notes.internal.category.repository.CategoryRepository;
import com.daily.notes.internal.history.model.entity.NotesHistory;
import com.daily.notes.internal.notes.model.entity.Notes;
import com.daily.notes.internal.notes.model.entity.UserNotes;
import com.daily.notes.internal.notes.model.request.UpdateNotes;
import com.daily.notes.internal.notes.model.response.NotesResponse;
import com.daily.notes.internal.notes.repository.NotesRepository;
import com.daily.notes.internal.user.model.entity.User;
import com.daily.notes.internal.user.model.response.UserResponse;
import com.daily.notes.internal.user.repository.UserRepository;
import com.daily.notes.utils.DateUtils;
import com.daily.notes.utils.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;
    private final NotesRepository notesRepository;

    public Response insertCategory(CategoryRequest request, String username) {
        Response response = new Response();
        try {
            User user = userRepository.getByUsername(username).get(0);
            Category category = new Category()
                    .setUserId(user.getId())
                    .setName(request.getName());
            categoryRepository.insertCategory(category);
            return response.success("Success insert category");
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response getCategoryById(String id) {
        Response response = new Response();
        try {
            List<UserCategory> userCategoryList = categoryRepository.getUserCategoryById(id);
            if(userCategoryList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);

            UserCategory userCategory = userCategoryList.get(0);
            UserResponse user = new UserResponse().setId(userCategory.getUserId()).setUsername(userCategory.getUserName());
            CategoryResponse category = new CategoryResponse()
                    .setId(userCategory.getId())
                    .setName(userCategory.getName())
                    .setUser(user);
            return response.success(category);
        }catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        }catch (Exception e){
            return response.error(e.getMessage());
        }
    }

    public Response getCategoryByUserId(String username) {
        Response response = new Response();
        try {
            String userId = userRepository.getByUsername(username).get(0).getId();
            List<UserCategory> userCategoryList = categoryRepository.getUserCategoryByUserId(userId);
            if(userCategoryList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);

            List<CategoryResponse> categoryResponseList = userCategoryList.stream()
                    .map(userCategory -> {
                        UserResponse user = new UserResponse().setId(userCategory.getUserId()).setUsername(userCategory.getUserName());
                        return new CategoryResponse()
                                .setId(userCategory.getId())
                                .setName(userCategory.getName())
                                .setUser(user);
                    }).toList();

            return response.success(categoryResponseList);
        }catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        }catch (Exception e){
            return response.error(e.getMessage());
        }
    }

    public Response updateCategory(UpdateCategory request) {
        Response response = new Response();
        try {
            List<Category> categoryList = categoryRepository.getCategoryById(request.getId());
            if(categoryList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);
            Category notes = new Category()
                    .setId(request.getId())
                    .setName(request.getName());
            categoryRepository.updateCategory(notes);
            return response.success("Success update category data");
        } catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }

    public Response deleteCategoryById(String id){
        Response response = new Response();
        try {
            List<Category> categoryList = categoryRepository.getCategoryById(id);
            if (categoryList.isEmpty()) throw new CustomException("Data not Found", Constant.ERROR_404, HttpStatus.NOT_FOUND);

            List<Notes> notesList = notesRepository.getNotesByCategoryId(id);
            if(!notesList.isEmpty()){
                notesRepository.updateNotesCategoryByCategoryId(id);
            }
            categoryRepository.deleteCategoryById(id);
            return response.success("Success delete category data");
        } catch (CustomException e){
            return response.error(e.getMessage(), e.getCode(), e.getHttpStatus());
        } catch (Exception e) {
            return response.error(e.getMessage());
        }
    }
}
