package com.daily.notes.internal.category.model.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Category {
    private String id;
    private String name;
    private String userId;
}
