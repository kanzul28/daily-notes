package com.daily.notes.internal.category.controller;

import com.daily.notes.internal.category.model.request.CategoryRequest;
import com.daily.notes.internal.category.model.request.UpdateCategory;
import com.daily.notes.internal.category.service.CategoryService;
import com.daily.notes.utils.Response;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
@Validated
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    private final String REQUEST_RECEIVED = "[{}][REQUEST RECEIVED][{}]";
    private final String REQUEST_END = "[{}][REQUEST END][{}]";

    @Operation(summary = "Insert Category", operationId = "insertCategory", description = "")
    @RequestMapping(method = RequestMethod.POST, value = "/category")
    public ResponseEntity<Response> insertCategory(@RequestBody @Valid CategoryRequest request,
                                                HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "insertCategory", servletRequest.getQueryString());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Response body =  categoryService.insertCategory(request, username);
        log.info(REQUEST_END, "insertCategory",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Get Category By User", operationId = "getCategoryByUser", description = "")
    @RequestMapping(method = RequestMethod.GET, value = "/category/user")
    public ResponseEntity<Response> getCategoryByUser(HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "getCategoryByUser", servletRequest.getQueryString());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Response body =  categoryService.getCategoryByUserId(username);
        log.info(REQUEST_END, "getCategoryByUser",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Get Category By Id", operationId = "getCategoryById", description = "")
    @RequestMapping(method = RequestMethod.GET, value = "/category/{id}")
    public ResponseEntity<Response> getCategoryById(@PathVariable String id,
                                             HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "getCategoryById", servletRequest.getQueryString());
        Response body =  categoryService.getCategoryById(id);
        log.info(REQUEST_END, "getCategoryById",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Update Category", operationId = "updateCategory", description = "")
    @RequestMapping(method = RequestMethod.PUT, value = "/category")
    public ResponseEntity<Response> updateNotes(
            @RequestBody UpdateCategory request
            , HttpServletRequest servletRequest) {
        log.info(REQUEST_RECEIVED, "updateCategory", servletRequest.getQueryString());
        Response body =  categoryService.updateCategory(request);
        log.info(REQUEST_END, "updateCategory",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }

    @Operation(summary = "Delete Category", operationId = "deleteCategory", description = "")
    @RequestMapping(method = RequestMethod.DELETE, value = "/category/{id}")
    public ResponseEntity<Response> deleteNotes(HttpServletRequest servletRequest
            , @PathVariable String id) {
        log.info(REQUEST_RECEIVED, "deleteCategory", servletRequest.getQueryString());
        Response body =  categoryService.deleteCategoryById(id);
        log.info(REQUEST_END, "deleteCategory",body.getHttpStatus());
        return new ResponseEntity<>(body, body.getHttpStatus());
    }
}
