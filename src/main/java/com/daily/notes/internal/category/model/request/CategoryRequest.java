package com.daily.notes.internal.category.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CategoryRequest {
    @NotBlank
    private String name;
}
