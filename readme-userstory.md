## user stories

- [x] user can register

```shell
curl --location 'localhost:8070/api/v1/user/signup' \
--header 'Content-Type: application/json' \
--data '{
    "username":"user",
    "password": "user123"
}'
```

- [x] user can login

```shell
curl --location 'localhost:8070/api/v1/user/login' \
--header 'Content-Type: application/json' \
--data '{
    "username":"user",
    "password": "user123"
}'
```

- [x] user can add new notes

```shell
curl --location 'http://localhost:8070/api/v1/notes' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--header 'Authorization: {{jwtToken}}' \
--data '{
  "title": "travel to england",
  "content": "today i will go to manchester!"
}'
```

- [x] user can see notes that already add

```shell
curl --location 'http://localhost:8070/api/v1/notes/bdf4096f-4924-47d5-b67b-ef0265a9e804' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--header 'Authorization: {{jwtToken}}'
```

```shell
curl --location 'http://localhost:8070/api/v1/notes?pageNo=0&pageSize=5' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--header 'Authorization: {{jwtToken}}'
```

```shell
curl --location 'http://localhost:8070/api/v1/notes/byDate?pageNo=1&pageSize=10' \
--header 'accept: */*' \
--header 'startDate: 2024-03-22 10:18:00' \
--header 'endDate: 2024-03-22 23:45:00' \
--header 'Authorization: {{jwtToken}}'
```

- [x] user can update existing notes

```shell
curl --location --request PUT 'http://localhost:8070/api/v1/notes' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--header 'Authorization: {{jwtToken}} \
--data '{
  "id": "fbc8ae57-2e92-466b-97a1-ede13c372f56",
  "title": "halo halo",
  "content": "halo dunia"
}'
```

- [x] user can delete existing notes

```shell
curl --location --request DELETE 'http://localhost:8070/api/v1/notes/bdf4096f-4924-47d5-b67b-ef0265a9e804' \
--header 'accept: */*' \
--header 'Authorization: {{jwtToken}}'
```

- [x] user can add new category

```shell
curl --location 'http://localhost:8070/api/v1/category' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--header 'Authorization: {{jwtToken}}' \
--data '{
  "name": "Financial"
}'
```

- [x] user can update their existing category

```shell
curl --location 'http://localhost:8070/api/v1/category' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--header 'Authorization: {{jwtToken}}' \
--data '{
  "name": "Financial"
}'
```

- [x] user can see their category

```shell
curl --location 'http://localhost:8070/api/v1/category' \
--header 'accept: */*' \
--header 'Content-Type: application/json' \
--header 'Authorization: {{jwtToken}}' \
--data '{
  "name": "Financial"
}'
```

```shell
curl --location 'http://localhost:8070/api/v1/category/9c17a001-9898-40a0-970b-7c24db4ba50d' \
--header 'accept: */*' \
--header 'Authorization: {{jwtToken}}'
```

- [x] user can delete existing category
```shell
curl --location --request DELETE 'http://localhost:8070/api/v1/category/abcdf' \
--header 'accept: */*' \
--header 'Authorization: Bearer {{jwtToken}}'
```

- [x] user can see their notes history 

```shell
curl --location 'http://localhost:8070/api/v1/history/74e281f2-e48e-4af7-bb47-5c3424e2de7a' \
--header 'accept: */*' \
--header 'Authorization: {{jwtToken}}'
```

```shell
curl --location 'http://localhost:8070/api/v1/history/notes/fbc8ae57-2e92-466b-97a1-ede13c372f56' \
--header 'accept: */*' \
--header 'Authorization: Bearer {{jwtToken}}'
```
